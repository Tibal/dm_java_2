import java.util.*;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer minElem = null;
        for(int i=0;i<liste.size();i++){
            if( minElem == null || liste.get(i) <= minElem){
                minElem = liste.get(i);
            }
        }
        return minElem;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if(liste.size() == 0){ return true ;}
        return valeur.compareTo(Collections.min(liste))<0;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> liste3 = new ArrayList<>();
        int cptListe1 = 0;
        int cptListe2 = 0;
        if(liste1.size()>0 && liste2.size()>0){
            while(cptListe1<liste1.size() && cptListe2<liste2.size() ){
                if(liste2.contains(liste1.get(cptListe1))){
                    if(! liste3.contains(liste1.get(cptListe1))){
                        liste3.add( liste1.get(cptListe1) );
                    }
                }
                if(  liste1.contains(liste2.get(cptListe2))  ){
                    if(!  liste3.contains(liste2.get(cptListe2))  ){
                        liste3.add( liste2.get(cptListe2) );
                    }
                }
                cptListe1++;
                cptListe2++;
            }
            Collections.sort(liste3);
        }
        return liste3;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeMots = new ArrayList<>();
        String[] espace = texte.split(" ");
        for(int i=0; i< espace.length; ++i){
            if(! espace[i].equals("")){
                listeMots.add(espace[i]);
            }
        }
        return listeMots; 
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        Integer resNombre = null;
        String resMot = ("");
        Integer cpt = null;
        List<String>listeMots = new ArrayList<>();
        String[] espace = texte.split(" ");
        for(int i=0; i<espace.length;++i){
            if(! espace[i].equals("")){
                listeMots.add(espace[i]);
            }
        }
        Collections.sort(listeMots);
        for(String mot:listeMots){
            cpt = Collections.frequency(listeMots,mot);
            if(resNombre == null || cpt > resNombre){
                resNombre = Collections.frequency(listeMots,mot);
                resMot = mot;
            }
            if(resNombre > listeMots.size()/2 ){
                return resMot;
            }

        }
        if( resMot.length() > 0){ return resMot; }
        return null;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cptParenthese = 0;
        for(int i=0;i<chaine.length();i++){
            if(chaine.charAt(i) == '('){cptParenthese += 1;}
            if(chaine.charAt(i) == ')'){
                if(cptParenthese <= 0){
                    return false;
                }
                else{cptParenthese -= 1;}
            }
        }
        return cptParenthese == 0;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        
        int cptParenthese = 0;
        int cptCrochets = 0;
        int testMemeParenthese = 0;
        for(int i=0;i<chaine.length();i++){

            if(chaine.charAt(i) == '('){cptParenthese += 1;}

            else if(chaine.charAt(i) == ')'){
                if(cptParenthese <= 0){
                    return false;
                }
                else{
                    cptParenthese -= 1;
                }
            }
            else if(chaine.charAt(i) == '['){
                cptCrochets += 1;
                testMemeParenthese = cptParenthese;
            }

            else if(chaine.charAt(i) == ']'){
                if(cptParenthese == testMemeParenthese){
                    cptCrochets -= 1;
                }
                else{
                    return false;
                }
            }
            
        }
        return cptParenthese == 0 && cptCrochets == 0;
        

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.size()>0){
            int indiceMini = 0;
            int indiceMax=liste.size();
            int indiceMilieu;
            while(indiceMini<indiceMax){
                indiceMilieu = ((indiceMini+indiceMax)/2);
                if(valeur.compareTo(liste.get(indiceMilieu))==0){
                    return true;
                }
                else if(valeur.compareTo(liste.get(indiceMilieu))>0){
                    indiceMini = indiceMilieu+1;
                }
                else{
                    indiceMax = indiceMilieu;
                }
            }
        }
        return false;



    }



}
